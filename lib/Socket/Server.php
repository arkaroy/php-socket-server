<?php

namespace Socket;

class Server extends Socket
{
    protected $clients = array();

    private $changed = array();
    private $hooks = array();
    private $iteration = 0;

    public function __construct($host, $port)
    {
        parent::__construct($host, $port);
        $this->log("Server Created.");
    }

    public function run()
    {
        while (true) {

            $this->iteration += 1;

            $this->clean();
            $this->select();

            foreach ($this->changed as $socket) {
                if ($socket == $this->master) {
                    // New connection
                    if (($resource = socket_accept($this->master)) === false) {
                        $this->error();
                        continue;
                    }
                    $client = new Client($this, $resource);
                    $this->clients[(int)$resource] = $client;
                    $this->sockets[] = $resource;
                    $this->trigger('connect', array($client));
                } else {
                    $client = $this->clients[(int)$socket];
                    if (!is_object($client)) {
                        unset($this->clients[(int)$socket]);
                        $this->close($socket);
                        continue;
                    }
                    $data = $this->read($socket);
                    if($data === false || strlen($data) < 1) {
                        unset($this->clients[(int)$socket]);
                        $this->close($socket);
                        continue;
                    } else {
                        $this->trigger('data', array($client, $data));
                        $parts = explode(' ', trim($data), 2);
                        $command = strtolower($parts[0]);
                        $this->trigger('command', array($client, $command, isset($parts[1]) ? $parts[1] : null));
                        $this->trigger($command, array($client, isset($parts[1]) ? $parts[1] : null));
                    }
                }
            } // end foreach

        }
    }

    public function notify($user_id, $msg) {
        foreach($this->clients as $client) {
            if($client->getUserId() == $user_id) {
                $client->send($msg);
            }
        }
    }

    protected function select()
    {
        $this->changed = $this->sockets;
        if (socket_select($this->changed, $write, $except, null) === false) {
            $this->error();
        }
    }

    public function clean()
    {
        foreach ($this->sockets as $i => $socket) {
            $timeout = array(
                "sec" => 0,
                "usec" => 100
            );
            if (socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, $timeout) === false) {
                $this->close($socket);
            }
        }
    }

    public function disconnect($client)
    {
        $this->trigger('disconnect', array($client));
        $resource = $client->socket();
        $this->close($resource);
        unset($this->clients[(int)$resource]);
    }

    public function status() {
        return array(
            'iteration' => $this->iteration,
            'sockets' => count($this->sockets),
            'clients' => count($this->clients)
        );
    }

    public function trigger($command, $args = array())
    {
        $command = strtolower($command);
        if (!isset($this->hooks[$command]) || !is_array($this->hooks[$command])) {
            return;
        }
        foreach ($this->hooks[$command] as $function) {
            call_user_func_array($function, $args);
        }
    }

    public function on($command, $function)
    {
        $command = strtolower($command);
        if (!isset($this->hooks[$command]) || !is_array($this->hooks[$command])) {
            $this->hooks[$command] = array();
        }
        if (array_search($function, $this->hooks[$command]) === false) {
            $this->hooks[$command][] = $function;
        }
    }

    public function off($command, $function)
    {
        $command = strtolower($command);
        if (!isset($this->hooks[$command]) || !is_array($this->hooks[$command])) {
            return;
        }
        if (($key = array_search($function, $this->hooks[$command])) !== false) {
            unset($this->hooks[$command][$key]);
        }
    }
}
