<?php

namespace Socket;

class Socket
{
    protected $master;
    protected $sockets = array();

    private $host;
    private $port;

    public function __construct($host, $port)
    {
        $this->host = $host;
        $this->port = $port;
        $this->create();
        $this->bind();
        $this->listen();
    }

    protected function create()
    {
        if (($this->master = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            $this->error();
            die();
        }
        socket_set_option($this->master, SOL_SOCKET, SO_REUSEADDR, 1);
        $this->sockets[] = $this->master;
        $this->log("Socket Created.");
    }

    protected function bind()
    {
        if (socket_bind($this->master, $this->host, $this->port) === false) {
            $this->error();
            die();
        }
        $this->log("Socket Bound.");
    }

    protected function listen()
    {
        if (socket_listen($this->master, 10) === false) {
            $this->error();
            die();
        }
        $this->log("Socket Listening on {$this->host}:{$this->port}.");
    }

    public function write($socket, $string)
    {
        if (socket_write($socket, $string) === false) {
            $this->error();
        }
    }

    public function read($socket)
    {
        $string = '';
        while ($read = socket_recv($socket, $buffer, 4, 0)) {
            $string .= $buffer;
        }
        return $string;
    }

    public function close($socket)
    {
        socket_close($socket);
        if(($k = array_search($socket, $this->sockets)) !== false) {
            unset($this->sockets[$k]);
        }
    }

    protected function error()
    {
        $code = socket_last_error();
        $msg = socket_strerror($code);
        $this->log("ERROR [{$code}]: {$msg}");
    }

    protected function log($msg)
    {
        echo "[" . date('Y-m-d H:i:s') . "] {$msg}\r\n";
    }
}
