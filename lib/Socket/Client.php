<?php

namespace Socket;

class Client
{
    private $server;
    private $socket;
    private $addr = null;
    private $port = null;
    private $time = null;
    private $id = null;
    private $user_id = 0;

    public function __construct($server, $socket)
    {
        $this->server = $server;
        $this->socket = $socket;
        if (socket_getpeername($this->socket, $addr, $port)) {
            $this->addr = $addr;
            $this->port = $port;
        }
        $this->time = microtime(true);
        $this->id = md5($this->addr . $this->port . $this->time . spl_object_hash($this));
    }

    public function send($string)
    {
        $this->server->write($this->socket, $string);
    }

    public function disconnect()
    {
        $this->server->disconnect($this);
    }

    public function id()
    {
        return $this->id;
    }

    public function socket()
    {
        return $this->socket;
    }

    public function server()
    {
        return $this->server;
    }

    public function addr()
    {
        return $this->addr;
    }

    public function port()
    {
        return $this->port;
    }

    public function time()
    {
        return $this->time;
    }

    public function setUserId($id) {
        $this->user_id = $id;
    }

    public function getUserId() {
        return $this->user_id;
    }
}
