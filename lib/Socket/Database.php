<?php

namespace Socket;

class Database {

    private $dbhost = '';
    private $dbname = '';
    private $dbuser = '';
    private $dbpass = '';
    private $pdo;
    private $stmt;
    private $connected = false;

    function __construct($name, $user, $pass, $host = 'localhost') {
        $this->dbhost = $host;
        $this->dbname = $name;
        $this->dbuser = $user;
        $this->dbpass = $pass;
        $this->connect();
    }

    private function connect() {
        $dsn = 'mysql:host=' . $this->dbhost . ';dbname=' . $this->dbname . '';
        $options = array(
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            \PDO::ATTR_PERSISTENT => true,
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES => false,
        );
        try {
            $this->pdo = new \PDO($dsn, $this->dbuser, $this->dbpass, $options);
            $this->connected = true;
        } catch (\PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function query($query, $params = array(), $fetchmode = \PDO::FETCH_ASSOC) {
        if (!$this->connected) {
            $this->connect();
        }
        try {
            $query = trim(str_replace("\r", " ", $query));
            $this->prepare($query);
            // $this->bindMore($params);
            $this->execute($params);
            $raw_query = explode(" ", preg_replace("/\s+|\t+|\n+/", " ", $query));
            $stmt = strtolower($raw_query[0]);
            if ($stmt === 'select' || $stmt === 'show') {
                return $this->stmt->fetchAll($fetchmode);
            } elseif ($stmt === 'insert') {
                return $this->lastInsertId();
            } elseif ($stmt === 'update' || $stmt === 'delete') {
                return $this->rowCount();
            } else {
                return NULL;
            }
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function prepare($query) {
        $this->stmt = $this->pdo->prepare($query);
    }

    public function execute($params = array()) {
        return $this->stmt->execute($params);
    }

    public function bindMore($params) {
        if (!is_array($params)) {
            return;
        }
        foreach ($params as $param => $value) {
            $this->bind($param, $value);
        }
    }

    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = \PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = \PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = \PDO::PARAM_NULL;
                    break;
                default:
                    $type = \PDO::PARAM_STR;
            }
        }
        if (substr($param, 0, 1) != ':') {
            $param = ':' . $param;
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function rowCount() {
        return $this->stmt->rowCount();
    }

    public function lastInsertId() {
        return $this->pdo->lastInsertId();
    }

    public function beginTransaction() {
        return $this->pdo->beginTransaction();
    }

    public function executeTransaction() {
        return $this->pdo->commit();
    }

    public function cancelTransaction() {
        return $this->pdo->rollBack();
    }

    public function close() {
        $this->pdo = null;
    }

}
