<?php

error_reporting(E_ALL);
set_time_limit(0);
ob_implicit_flush();
date_default_timezone_set('Asia/Kolkata');

require_once 'lib/Socket/Socket.php';
require_once 'lib/Socket/Server.php';
require_once 'lib/Socket/Client.php';
require_once 'lib/Socket/Database.php';

define("DB_HOST", "localhost");
define("DB_NAME", "tradonli_doclocker");
define("DB_USER", "tradonli_doclock");
define("DB_PASS", "ankitdoclocker2017");

$host = "162.251.86.77";
// $host = "192.168.0.101";
$port = 37582;

$server = new \Socket\Server($host, $port);

// Triggered when new client connects
$server->on('connect', function ($client) {
    $addr = $client->addr();
    $port = $client->port();
    d("CONN: {$addr}:{$port} #" . $client->id() . " @ " . (int)$client->socket());
    $client->send("WLCM\n");
});

// Triggered when client sends data
$server->on('data', function ($client, $data) {
    // TODO: data received from client
    d("RCVD #" . $client->id() . " @ " . (int)$client->socket() . ": " . trim($data));
});

// Triggered when client is about to disconnect
$server->on('disconnect', function ($client) {
    d("DSCN: #" . $client->id() . " @ " . (int)$client->socket());
    $client->send("GDBY\n");
});

// Triggered when received command from client
$server->on('command', function ($client, $command, $args) {
    if ($command == 'quit' || $command == 'exit') {
        $client->disconnect();
    }
});

$server->on('ping', function ($client, $args) {
    $client->send("PING_RECEIVED\n");
});

$server->on('echo', function ($client, $string) {
    $client->send($string . "\n");
});

$server->on('server', function ($client, $args) {
    $status = $client->server()->status();
    $str = '';
    foreach ($status as $k => $v) {
        $str .= "{$k} = {$v}\n";
    }
    $client->send($str);
});

$server->on('notify', function ($client, $id) {
    $client->server()->notify($id, "update\n");
});

$server->on('auth', function ($client, $token) {
    $token = trim($token);
    if ($token) {
        $user_id = authorize($token);
        if($user_id) {
            $client->setUserId($user_id);
            $client->send("REG #{$user_id}\n");
            if(isNew($user_id)) {
                $client->send("update\n");
            }
        } else {
            $client->disconnect();
        }
    } else {
        $client->send("ERROR\n");
    }
});

$server->run();

function authorize($token)
{
    // TODO: verify auth token and return user id
    $db = new Socket\Database(DB_NAME, DB_USER, DB_PASS, DB_HOST);
    $query = $db->query("SELECT * FROM sessions WHERE token = ?", array($token));
    if($db->rowCount() > 0) {
        return $query[0]['user_id'];
    }
    return 0;
}

function isNew($user_id) {
    return (int) newCount($user_id) > 0;
}

function newCount($user_id) {
    $db = new Socket\Database(DB_NAME, DB_USER, DB_PASS, DB_HOST);
    $sql = "SELECT COUNT(*) as new_count FROM `user_docs` WHERE `status` = ? AND `user_id` = ?";
    $params = array(2, $user_id);
    $query = $db->query($sql, $params);
    if($db->rowCount() > 0) {
        return $query[0]['new_count'];
    }
    return 0;
}

function d($msg)
{
    echo "[" . date('Y-m-d H:i:s') . "] {$msg}\n";
}
